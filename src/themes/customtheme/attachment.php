<?php
/**
 * @package WordPress
 * @subpackage Custom_Theme
 */

get_header(); ?>
<div id="content" class="row">
    <div id="main-content" class="col-xs-12 col-sm-9">
            <h2>Attachment</h2>
<?php get_template_part( 'loop', 'attachment' ); ?>
    </div><!-- /main-content -->
<?php get_sidebar(); ?>
</div><!-- /content -->
<?php get_footer(); ?>