<?php
/**
 * @package WordPress
 * @subpackage Custom_Theme
 */

get_header(); ?>
<div id="content" class="row">
    <div id="main-content" class="col-xs-12 col-sm-9">
            <h2>Latest Stories</h2>
<?php get_template_part( 'loop', 'index' ); ?>
    </div><!-- /main-content -->
<?php get_sidebar(); ?>
</div><!-- /content -->
<?php get_footer(); ?>