<?php
/**
 * @package WordPress
 * @subpackage Custom_Theme
 */
?>
<form method="get" name="searchform" id="searchform" class="form-inline" action="<?php bloginfo('url'); ?>/">
<div class="form-group">
<label class="sr-only" for="s">Search</label>
<input type="text" class="form-control" value="<?php the_search_query(); ?>" name="s" id="s" />
</div>
<button type="submit" id="searchsubmit" class="btn btn-default button-submit">Search</button>
</form>
