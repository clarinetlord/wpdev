<?php 

/**
 * Sets active plugins and runs Wordpress test framework
 */

define( 'WP_DEV_ABSPATH_TEST', realpath( '.' ) );
define( 'WP_DEV_PROPERTIES_FILE', WP_DEV_ABSPATH_TEST .'/'. WPDEV_PROPERTIES_FILE );

class WP_Dev_Test_Bootstrap {
    
    /**
     * Holds parses properties from file
     * 
     * @var array
     */
    private $properties;
    
    /**
     * Absolute path to the includes directory for the WP test framework
     * 
     * @var string
     */
    private $wp_tests_include_path;
    
    /**
     * Absolute path to plugin tests directory
     * 
     * @var string
     */
    private $plugins_test_path;
    
    /**
     * Relative paths for all active plugin main files
     * 
     * @var array
     */
    private $plugin_paths;
    
    /**
     * Constructor
     */
    function __construct() {
        $this->setup_properties();
        $this->bootstrap();
    }
    
    /**
     * Parses and loads settings from properties file 
     */
    private function setup_properties() {
        $this->properties = parse_ini_file( WP_DEV_PROPERTIES_FILE );
        $this->wp_tests_include_path = WP_DEV_ABSPATH_TEST . '/' . $this->properties['local.wp.test.includespath'];
        $this->plugins_test_path = WP_DEV_ABSPATH_TEST . '/' . $this->properties['local.plugins.test.path'];
    }
    
    /**
     * Hooks preload_active_plugins and loads Wordpress test framework bootstrap
     */
    private function bootstrap() {
        require_once ( $this->wp_tests_include_path . '/functions.php' );
        tests_add_filter( 'muplugins_loaded' , array( $this, 'preload_active_plugins_and_tests' ) );
        //load blank key so proper hook is created in WP bootstrap
        $GLOBALS['wp_tests_options'] = array( 'active_plugins' => array() );
        require_once $this->wp_tests_include_path . '/bootstrap.php';
        $this->include_plugin_tests_setup();
    }
    
    /**
     * Scans plugins directory and sets all plugin files to activate during tests and adds plugin tests' setup files to include
     * 
     * action: muplugins_loaded
     */
    function preload_active_plugins_and_tests() {
        require_once ABSPATH . 'wp-admin/includes/plugin.php';
        $plugins = get_plugins();
        echo "Loading plugins:\n";
        foreach ( $plugins as $name => $plugin_data ) {
            echo "--{$plugin_data['Name']} {$plugin_data['Version']}\n";
        }
        $this->plugin_paths = array_keys( $plugins );
        $GLOBALS['wp_tests_options'] = array( 'active_plugins' => $this->plugin_paths );
    }
    
    /**
     * Includes setup files for all active plugins with test suites
     * 
     */
    function include_plugin_tests_setup() {
        foreach ( $this->plugin_paths as $path ) {
            $test_setup_file = $this->plugins_test_path . '/' . $path;
            if ( file_exists( $test_setup_file ) ) require_once $test_setup_file;
        }
    }
}

new WP_Dev_Test_Bootstrap();

?>