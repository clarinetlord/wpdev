<?php

/*
Plugin Name: Sample Plugin
Plugin URI: http://www.collierwebdesign.com/
Description: Sample WordPress plugin for the WP Dev framework
Version: 1.0
Author: Cyrus Collier
*/

class Sample_Plugin {
    
    function __construct() {
        add_action('plugins_loaded',array(&$this,'plugins_loaded'));
        add_action('init',array(&$this,'init'));
    }
    
    function plugins_loaded() {}
    function init() {}
    
}

$GLOBALS['sample-plugin'] = new Sample_Plugin();