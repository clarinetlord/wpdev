<?php

namespace WPDev\Theme;

class Util {
    
    static function getNestedShortcodeContent( $content ) {
        $content = preg_replace( '@^(<br />|</p>)?@s', '', $content ); //orphaned tags at start of content
        $content = preg_replace( '@(<br />|<p>)?$@s', '', $content ); //orphaned tags at end of content
        $content = preg_replace( '@\]\s*<br />\s*\[@s', "]\r\n[", $content ); //undo <br /> between shortcodes
        return do_shortcode( shortcode_unautop( $content ) );
    }
}