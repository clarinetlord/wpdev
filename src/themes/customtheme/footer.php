<?php
/**
 * @package WordPress
 * @subpackage Custom_Theme
 */
?>

<footer id="footer">
    <?php wp_nav_menu( array( 'container' => '', 'theme_location' => 'footer' ) ); ?>
    <div class="col-xs-12 col-sm-2">
        <p><a href="<?php bloginfo( 'home' ); ?>"><?php echo WPDev\Theme\Options::getImage( 'footer_logo' ); ?></a></p>
    </div>
    <div class="col-sm-10">
        <?php echo WPDev\Theme\Options::get( 'footer_html' ); ?>
    </div>
</footer><!-- /footer -->
</div><!-- /container -->
<?php wp_footer(); ?>
</body>
</html>
