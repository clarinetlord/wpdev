<?php
/**
 * @package WordPress
 * @subpackage Custom_Theme
 */

/* Single Post / Page loop template */

?>
    <?php if (have_posts()) : ?>
    
        <?php while (have_posts()) : the_post(); ?>
            <?php if ( is_single()): ?>
            <div class="navigation row">
                <div class="previous col-xs-12 col-sm-4"><?php previous_post_link('&laquo; %link') ?></div>
                <div class="next col-xs-12 col-sm-4 col-sm-offset-4"><?php next_post_link('%link &raquo;') ?></div>
            </div>
            <?php endif; ?>
            <div id="post-<?php the_ID(); ?>" <?php post_class( 'col-sm-12' ); ?>>
            <h2><?php the_title(); ?></h2>
            <div class="entry">
                <?php the_content(); ?>
            </div>
            </div>
                <?php wp_link_pages(array('before' => '<div class="navigation"><div class="previous">Pages: ', 'after' => '</div></div>', 'next_or_number' => 'number')); ?>
            <div class="col-sm-12">
                <?php the_tags( '<p>Tags: ', ', ', '</p>'); ?>
                <?php if(is_single()): ?>
                <p class="postmetadata">
                        This entry was posted on <?php the_time('l, F jS, Y') ?> at <?php the_time() ?> and is filed under <?php the_category(', ') ?>.
                        You can follow any responses to this entry through the <?php post_comments_feed_link('RSS 2.0'); ?> feed.
                    
                </p>
                <?php endif; ?>
            </div>
                <?php edit_post_link('Edit this entry','<p>','</p>'); ?>
            <div class="col-sm-12">
            <?php comments_template(); ?>
            </div>
        <?php endwhile; ?>
        
    <?php else : ?>
    
            <div class="page">
            <h2 class="center-block">Not Found</h2>
            <p class="center-block">Sorry, but you are looking for something that isn't here.</p>
            <?php get_search_form(); ?>
            </div>

    <?php endif; ?>
