<?php global $block; /* @var $block \WPDev\Theme\Block\Block */ ?>
            <div<?php echo $block->getAttributes(); ?>>
                <div class="col-sm-12">
                    <?php get_template_part( 'blocks/_title' ); ?>
                </div>
                <div class="<?php echo $block->getContentCSSClass( 'col-sm-12' ); ?>">
                <?php echo $block->getContent(); ?>
                </div>
            </div>
