<?php

namespace WPDev\Theme\Bootstrap;

use WPDev\Theme\Base\BaseController;

class BootstrapController extends BaseController {
        
    protected function hooks() {
        add_filter( 'nav_menu_css_class', array( $this, 'addNavItemClasses' ) );
        add_filter( 'comment_form_default_fields',  array( $this, 'modifyCommentFormClasses' ) );
        add_filter( 'comment_form_field_comment',  array( $this, 'modifyCommentFormClasses' ) );
        add_filter( 'comment_form_field_comment', function( $f ) { ob_start(); return $f; } );
        add_action( 'comment_form', array( $this, 'replaceSubmitHTML' ) );
    }

    function addNavItemClasses( $classes ) {
        if ( in_array( 'current-menu-item', $classes )  ) $classes[] = 'active';
        if ( in_array( 'menu-item-has-children', $classes) ) $classes[] = 'dropdown';
        return $classes;
    }
    
    function modifyCommentFormClasses( $fields ) {
        if ( is_array( $fields ) ) $field_array =& $fields;
        else $field_array = array( &$fields );
        foreach ( $field_array as $name => $field ) {
            $field_array[$name] = str_replace(
                array( 'class="comment-form-',            ' name="' ),
                array( 'class="form-group comment-form-', ' class="form-control" name="' ),
                $field
            );
        }
        return $fields;
    }

    function replaceSubmitHTML() {
        $output = ob_get_clean();
        $output = preg_replace(
            '@<input (name=".*?") type="submit" (id=".*?") value="(.*?)" />@',
            '<button type="submit" class="btn btn-default" $1 $2>$3</button>',
            $output
        );
        echo $output;
    }
}