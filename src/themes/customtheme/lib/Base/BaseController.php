<?php

namespace WPDev\Theme\Base;

    
abstract class BaseController extends Singleton {
    
    static function init() {
        static $hooks_ran = false;
        if ( !$hooks_ran ) {
            static::getInstance()->hooks();
            $hooks_ran = true;
        }
    }

    abstract protected function hooks();
}
