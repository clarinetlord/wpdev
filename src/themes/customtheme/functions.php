<?php
/**
 * @package WordPress
 * @subpackage Custom_Theme
 */

require_once 'lib/autoload.php';

add_action( 'after_setup_theme', 'wpdev_theme_setup' );

function wpdev_theme_setup() {
    register_sidebar(array(
        'name' => 'Main Sidebar Widgets',
        'id' => 'main-sidebar-widgets',
        'before_widget' => '<div id="%1$s" class="widget %2$s"><div class="widget-container">',
        'after_widget' => '</div></div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>'
    ));

    register_nav_menus( array(
        'main' => __( 'Main Navigation', 'custom-theme' ),
        'footer' => __( 'Footer Navigation', 'custom-theme' )
    ) );
    
    add_theme_support( 'post-thumbnails' );
    
    \WPDev\Theme\Theme::init();
    
}

add_action( 'wp_enqueue_scripts', 'wpdev_theme_scripts' );

function wpdev_theme_scripts() {
    if ( is_singular() && get_option( 'thread_comments' ) )
        wp_enqueue_script( 'comment-reply' );
}

