<?php

namespace WPDev\Theme\Base;

abstract class CustomPostType {
    
	const POST_TYPE = null;
	const POST_GLOBAL_VAR = null;
    public $post;
    
    function __construct( $post_id ) {
        if ( $post_id ) {
            $this->post = get_post( $post_id );
            if ( !static::checkPostType( $this->post ) ) {
                $this->post = new WP_Error( 'incorrect post type used in initializing ' . get_called_class() );
                return;
            }
            $meta = get_post_custom( $this->post->ID );
            foreach ( $meta as $key => $values ) {
                if ( property_exists( $this, $key ) && !empty( $values[0] ) ) {
                    $val = $values[0];
                    $this->{$key} = $this->getProperty( $key, $val );
                }
            }
        }
    }
    
    protected function getProperty( $key, $val ) {
       return $val; 
    }
    
    function getContent() {
        return apply_filters( 'the_content', $this->post->post_content );
    }
    
    function getTitle() {
        return apply_filters( 'the_title', $this->post->post_title );
    }
    
    function render( $echo = false, $template = false ) {
        if ( !( $this->post && 'publish' == $this->post->post_status ) ) return;
        $global = !is_null( static::POST_GLOBAL_VAR ) ? static::POST_GLOBAL_VAR : static::POST_TYPE;
        if ( !$template ) $template = 'content/' . static::POST_TYPE;
        $GLOBALS[$global] = $this;
        ob_start();
    	get_template_part( $template );
    	$output = ob_get_clean();
    	if ( $echo ) echo $output;
    	else return $output;
    }
    
    static function getClassName() {
        $class_name = apply_filters( 'wpdev_class_name_' . static::POST_TYPE, false );
        return $class_name ? $class_name : get_called_class();
    }
    
    /**
     * @param int $post_id
     * @return self
     */
    static function getInstance( $post_id ) {
        $class_name = self::getClassName();
        return new $class_name( $post_id );
    }
    
    static function checkPostType( $post ) {
        return $post->post_type == static::POST_TYPE;
    }
    
    static function getPosts( $args = array() ) {
        $defaults = array(
            'posts_per_page' => -1, 
            'post_type' => static::POST_TYPE
        );
        $r = wp_parse_args( $args, $defaults );
        $get_posts = new WP_Query;
        $posts = get_posts( $r );
        $class = get_called_class();
        $objects = array();
        foreach( $posts as $post ) {
            $objects[] = new $class( $post->ID );
        }
        return $objects;
    }
    
    static function shortcode($atts, $content = null ) {
    	extract( shortcode_atts( array(
    		'slug' => '',
    	), $atts ) );
    	$post = get_page_by_path( $slug, ARRAY_A, static::POST_TYPE );
    	$class = get_called_class();
    	if ( $post && isset( $post['ID'] ) ) {
    		$cpt = new $class( $post['ID'] );
    		return $cpt->render( false );
    	} else {
    		return "<!-- $class not found for slug: $slug -->";
    	}
    }
}