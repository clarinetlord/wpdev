<?php 

namespace WPDev\Theme\Block;

use WPDev\Theme\Base\CustomPostType;

class Block extends CustomPostType {
    
    const POST_TYPE = 'block';
    const TAXONOMY_COLOR = 'color';
    const DEFAULT_COLOR_BACKGROUND = 'default';
    const DEFAULT_COLOR_ELEMENT = 'primary';
    const DEFAULT_TEMPLATE = 'basic';
    
    const FEATURED_IMAGE_POSITION_LEFT = 'left';
    const FEATURED_IMAGE_POSITION_RIGHT = 'right';
    const LOCATION_ABOVE = 'above';
    const LOCATION_BELOW = 'below';
    
    const MAX_CONTENT_WIDTH = 12;
    
    const META_KEY_TOP_BANNER_BLOCK = 'top_banner_block';
    
    public $post;
    
    protected $color_background = self::DEFAULT_COLOR_BACKGROUND;
    protected $color_element = self::DEFAULT_COLOR_ELEMENT;
    
    public $content_text_uses_element_color = false;
    public $template = self::DEFAULT_TEMPLATE;
    public $featured_image_position = self::FEATURED_IMAGE_POSITION_RIGHT;
    public $content_position = self::FEATURED_IMAGE_POSITION_LEFT;
    public $hide_title = false;
    public $content_width = 0;
    public $show_featured_image_on_mobile = false;
    
    public $in_block = false;
    public $is_top_banner = false;
    
    function __construct( $post_id ) {
        parent::__construct( $post_id );
        if ( $this->post ) {
            $banner_block_id = self::getTopBannerBlockId();
            if ( $banner_block_id && $banner_block_id == $this->post->ID ) $this->is_top_banner = true;
            if ( $this->featured_image_position == self::FEATURED_IMAGE_POSITION_LEFT )
                $this->content_position = self::FEATURED_IMAGE_POSITION_RIGHT;
        }
    }
    
    protected function getProperty( $key, $val ) {
        $val = parent::getProperty( $key, $val );
        return $this->isColorField( $key ) ?
            get_term_field( 'slug', $val, 'color') :
            $val;
    }
    
    function render( $echo = true ) {
        $this->in_block = true;
        $output = parent::render( false, "blocks/$this->template" );
        if ( !empty( $output ) && $url = get_edit_post_link( $this->post->ID ) ) {
        	$output .= '<a class="post-edit-link" href="' . $url . '">Edit Block</a>';
        }
        $this->in_block = false;
        if ( $echo ) echo $output;
        else return $output;
    }
    
    function getBackgroundColor() {
        return $this->color_background;
    }
    
    function getElementColor() {
        return self::isLightColor( $this->color_background ) ? $this->color_element : self::DEFAULT_COLOR_BACKGROUND;
    }
    
    function getFeaturedImage() {
        return get_the_post_thumbnail( $this->post->ID, 'full', $this->getImageAttributes() );
    }
    
    function getImageAttributes() {
        $image_atts = array( 'class' => 'img-responsive' );
        if ( !$this->show_featured_image_on_mobile ) $image_atts['class'] .= ' hidden-xs';
        return $image_atts;
    }
    
    function getPostElementColor() {
        return $this->color_background != self::DEFAULT_COLOR_BACKGROUND ?
            $this->color_background :
            self::DEFAULT_COLOR_ELEMENT;
    }
    
    function getCSSId() {
        return apply_filters( 'wpdev_block_css_id', sprintf( 'block-%s', $this->post->post_name ) );
    }
    
    function getCSSClass() {
        return apply_filters( 'wpdev_block_css_class', sprintf( 'block clearfix bg-%s element-%s', $this->getBackgroundColor(), $this->getElementColor() ) );
    }
    
    function getAttributes() {
        $attr = array();
        $attr['id'] = $this->getCSSId();
        $attr['class'] = $this->getCSSClass();
        $attr = apply_filters( 'wpdev_block_attributes', $attr );
        $output = '';
        foreach ( $attr as $name => $value ) {
    		$output .= sprintf( ' %s="%s"', $name, $value );
    	}
    	return $output;
    }
    
    function getContentCSSClass( $classes ) {
        if ( $this->content_text_uses_element_color ) $classes .= ' element-color';
        return apply_filters( 'wpdev_block_content_class', $classes );
    }
    
    function getImageColumnCSSClass( $classes = '' ) {
        $columns = $this->getColumns();
        $image_class = $columns['image_class'];
        if ( $classes ) $image_class = ' ' . $classes;
        $image_columns = $columns['image_columns'];
        $image_class = apply_filters( "wpdev_block_image_column_class_$this->featured_image_position", $image_class, $image_columns );
        return apply_filters( 'wpdev_block_image_column_class', $image_class, $image_columns );
        
    }
    
    function getContentColumnCSSClass( $classes = '' ) {
        $columns = $this->getColumns();
        $content_class = $columns['content_class'];
        if ( $classes ) $content_class = ' ' . $classes;
        $content_columns = $columns['content_columns'];
        $content_class = apply_filters( "wpdev_block_content_column_class_$this->content_position", $content_class, $content_columns );
        return apply_filters( 'wpdev_block_content_column_class', $content_class, $content_columns );
    }
    
    protected function getColumns() {
        static $columns;
        if ( !$columns ) {
            $content_columns = (int) $this->content_width;
            if ( !$content_columns ) $content_columns = 6;
            $image_columns = self::MAX_CONTENT_WIDTH - $content_columns;
            $content_class = $this->getContentCSSClass( "col-sm-$content_columns" );
            $image_class = "col-sm-$image_columns";
            $columns = compact( 'content_class', 'content_columns', 'image_class', 'image_columns' );
        }
        return $columns;
    }
    
    static function inBlock() {
        global $block;
        return is_a( $block, __CLASS__ ) && $block->in_block;
    }

    static function isLightColor( $color ) {
        return $color == self::DEFAULT_COLOR_BACKGROUND || false !== strpos( $color, 'light' );
    }
    
    static function isColorField( $key, $prefix = '' ) {
        $field = str_replace( $prefix, '', $key );
        return 0 === strpos( $field, 'color_' );
    }
    
    static function getTopBannerBlockId() {
        $banner_block = get_post_meta( get_the_ID(), self::META_KEY_TOP_BANNER_BLOCK, true );
        return isset( $banner_block['ID'] ) ? $banner_block['ID'] : false;
    }
    
}
