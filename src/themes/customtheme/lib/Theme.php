<?php

namespace WPDev\Theme;

use WPDev\Theme\Base\BaseController;

class Theme extends BaseController {
        
    function hooks() {
        Shortcodes::init();
        Bootstrap\BootstrapController::init();
        Block\BlockPodsController::init();
        Block\BlockViewController::init();
        $class = self::getInstance();
        add_action( 'wp_enqueue_scripts', array( $class, 'enqueueScripts' ) );
        add_action( 'admin_enqueue_scripts', array( $class, 'adminScripts' ) );
        
    }
    
    function enqueueScripts() {
        wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css', false, '4.2.0' );
        wp_register_style( 'bootstrap-css', get_template_directory_uri() . '/css/bootstrap.css', false, '3.2.0' );
        wp_register_script( 'bootstrap-js', get_template_directory_uri() . '/js/bootstrap.min.js', array( 'jquery' ), '3.2.0' );
        wp_enqueue_style( 'theme-css', get_template_directory_uri() . '/css/theme.css', array( 'bootstrap-css' ) );
        wp_enqueue_script( 'respond-js', get_template_directory_uri() . '/js/respond.js' );
        wp_enqueue_script( 'theme-js', get_template_directory_uri() . '/js/theme.js', array( 'bootstrap-js' ) );
    }
    
    
    function adminScripts() {
        wp_enqueue_style( 'admin-css', get_stylesheet_directory_uri() . '/css/admin.css' );
    }
}