<?php
/**
 * @package WordPress
 * @subpackage Custom_Theme
 */

get_header();
?>
<div id="content" class="row">
    <div id="main-content" class="col-xs-12 col-sm-9">
        <h2>
<?php if ( is_day() ) : ?>
                <?php printf( __( 'Daily Archives: <span>%s</span>', 'theltrainer' ), get_the_date() ); ?>
<?php elseif ( is_month() ) : ?>
                <?php printf( __( 'Monthly Archives: <span>%s</span>', 'theltrainer' ), get_the_date('F Y') ); ?>
<?php elseif ( is_year() ) : ?>
                <?php printf( __( 'Yearly Archives: <span>%s</span>', 'theltrainer' ), get_the_date('Y') ); ?>
<?php else : ?>
                <?php _e( 'Archives', 'theltrainer' ); ?>
<?php endif; ?>
        </h2>
<?php get_template_part( 'loop', 'archive' ); ?>
    </div><!-- /main-content -->
<?php get_sidebar(); ?>
</div><!-- /content -->
<?php get_footer(); ?>
