<?php

/**
 * PSR-4 compliant autoload for namespaced library classes
 */

define( 'WPDEV_THEME_NAMESPACE', 'WPDev\\Theme\\' );

spl_autoload_register( function ( $class ) {
    $len = strlen( WPDEV_THEME_NAMESPACE );
    if ( strncmp( WPDEV_THEME_NAMESPACE, $class, $len ) !== 0 ) return;
    $relative_class = substr( $class, $len );
    $file = __DIR__ . '/' . str_replace( '\\', '/', $relative_class ) . '.php';
    if ( file_exists( $file ) ) {
        require $file;
    }
} );