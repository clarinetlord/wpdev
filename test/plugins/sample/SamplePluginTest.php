<?php 

class SamplePluginTest extends WP_UnitTestCase {

    private $plugin;

    function setUp() {
        parent::setUp();
        $this->plugin = $GLOBALS['sample-plugin'];
    }

    function test_setup() {
        $this->assertFalse( null == $this->plugin );
    }

    function test_hook_plugins_loaded() {
        $this->assertGreaterThan(0,
                has_filter('plugins_loaded',array(&$this->plugin,'plugins_loaded'))
        );
    }

    function test_hook_init() {
        $this->assertGreaterThan(0,
                has_filter('init',array(&$this->plugin,'init'))
        );
    }
}

?>