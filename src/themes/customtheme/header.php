<?php
/**
 * @package WordPress
 * @subpackage Custom_Theme
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />

<title><?php wp_title('&laquo;', true, 'right'); ?> <?php bloginfo('name'); ?></title>

<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="alternate" type="application/atom+xml" title="<?php bloginfo('name'); ?> Atom Feed" href="<?php bloginfo('atom_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

<?php wp_head(); ?>

</head>
<body <?php body_class() ?>>
<div class="container">
<header class="row" id="header">
    <div class="col-xs-12">
        <h1 id="masthead"><a href="<?php bloginfo('url'); ?>"><?php echo WPDev\Theme\Options::getImage( 'header_logo' , get_bloginfo('name') ); ?></a></h1>
    </div>
</header><!-- /header -->
<nav class="navbar navbar-default" role="navigation">
<?php 
wp_nav_menu( array( 
    'container' => '',
    'menu_id' => 'main-navigation',
    'menu_class' => 'nav navbar-nav',
    'theme_location' => 'main',
    'menu' => 'name',
    'link_before' => '<span>',
    'link_after' => '</span>'
) );
?>
</nav>
<?php echo WPDev\Theme\Options::get( 'header_html' ); ?>
<!-- /navigation -->