<?php
/**
 * @package WordPress
 * @subpackage Custom_Theme
 */

/* Posts loop template */

?>
    <?php if (have_posts()) : ?>

        <?php while (have_posts()) : the_post(); ?>
            <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <h4><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>
                <p class="post-info">
                    <?php the_date('','','<br />'); ?>
                    <?php the_author(); ?>
                </p> 
                
                <?php the_excerpt(); ?>
                
                <p class="alignright"><a href="<?php the_permalink(); ?>">Read More &raquo;</a></p>
            </div>

        <?php endwhile; ?>
            <div class="navigation row">
                <div class="previous col-xs-12 col-sm-4"><?php next_posts_link(" &laquo; Older Entries") ?></div>
                <div class="next col-xs-12 col-sm-4 col-sm-offset-4"><?php previous_posts_link("Newer Entries &raquo;") ?></div>
            </div>

    <?php else : ?>

            <h2 class="center-block">Not Found</h2>
            <p class="center-block">Sorry, but you are looking for something that isn't here.</p>
            <?php get_search_form(); ?>

    <?php endif; ?>