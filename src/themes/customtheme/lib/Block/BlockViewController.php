<?php

namespace WPDev\Theme\Block;

use WPDev\Theme\Base\BaseController;

class BlockViewController extends BaseController {
    
    protected function hooks() {
        add_action( 'wpdev_render_blocks_above', array( $this, 'renderBlocksAbove' ) );
        add_action( 'wpdev_render_blocks_below', array( $this, 'renderBlocksBelow' ) );
    }
    
    function renderBlocksAbove() {
        $blockview = new BlockView( Block::LOCATION_ABOVE );
        $blockview->renderTopBannerBlock();
        $blockview->renderBlocks();
    }
    
    function renderBlocksBelow() {
        $blockview = new BlockView( Block::LOCATION_BELOW );
        $blockview->renderBlocks();
    }
}