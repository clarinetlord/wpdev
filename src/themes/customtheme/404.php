<?php
/**
 * @package WordPress
 * @subpackage Custom_Theme
 */

get_header(); ?>
<div id="content" class="row">
    <div id="main-content" class="col-xs-12 col-sm-9">
        <h2 class="center-block">Error 404 - Not Found</h2>
        <p class="center-block">Sorry, but you are looking for something that isn't here.</p>
        <?php get_search_form(); ?>
    </div><!-- /main-content -->
<?php get_sidebar(); ?>
</div><!-- /content -->
<?php get_footer(); ?>