<?php

namespace WPDev\Theme;

use WPDev\Theme\Block\Block;
use WPDev\Theme\Base\Singleton;

class Shortcodes extends Singleton {
    
    const SHORTCODE_TEMPLATE_PART = 'template_part';
    const SHORTCODE_BLOCK = 'block';
    const SHORTCODE_BUTTON = 'button';
    const SHORTCODE_LINK = 'link';
    const SHORTCODE_ROW = 'row';
    const SHORTCODE_SUBROW = 'subrow';
    const SHORTCODE_COL = 'col';
    const SHORTCODE_SUBCOL = 'subcol';
    const SHORTCODE_ICON = 'icon';
    const SHORTCODE_LARGEICON = 'largeicon';
    const SHORTCODE_RECENT_POSTS = 'recent_posts';
    
    static $shortcodes = array(
    	self::SHORTCODE_TEMPLATE_PART,
    	self::SHORTCODE_BLOCK,
    	self::SHORTCODE_BUTTON,
    	self::SHORTCODE_LINK,
    	self::SHORTCODE_ROW,
    	self::SHORTCODE_SUBROW,
    	self::SHORTCODE_COL,
    	self::SHORTCODE_SUBCOL,
    	self::SHORTCODE_ICON,
    	self::SHORTCODE_LARGEICON,
    	self::SHORTCODE_RECENT_POSTS,
    );
    
    static function init() {
        foreach ( self::$shortcodes as $shortcode ) {
            add_shortcode( $shortcode, array( self::getInstance(), $shortcode ) );
        }
    }

    function template_part( $atts, $content = null ) {
        extract( shortcode_atts( array(
            'slug' => 'default',
            'name' => null
        ), $atts ) );
        ob_start();
        get_template_part($slug, $name );
        return ob_get_clean();
    }
    
    function block( $atts, $content = null ) {
        $block_class = Block::getClassName();
        return call_user_func( array( $block_class, 'shortcode' ), $atts, $content ); 
    }
    
    function button( $atts, $content = null ) {
        global $block, $post_element_color;
        $color = Block::inBlock() ? $block->get_button_element_color() : $post_element_color;
        $atts = shortcode_atts( array(
            'link' => '#',
            'class' => "btn-$color",
            'submit_button' => ''
        ), $atts );
        $atts['class'] = 'btn ' . $atts['class'];
        if( empty( $atts['submit_button'] ) ) return $this->link( $atts, $content );
        ob_start();
        ?><button type="submit" class="<?php echo $atts['class'] ?>"><?php echo Util::getNestedShortcodeContent( $content ) ?></button><?php
        return ob_get_clean();
    }
        
    function link( $atts, $content = null ) {
        global $wpdb;
        extract( shortcode_atts( array(
            'link' => '#',
            'class' => ""
        ), $atts ) );
        $link_parts = explode( '#', $link );
        $link = $link_parts[0];
        $anchor = isset( $link_parts[1] ) ? '#' . $link_parts[1] : '';
        //generate internal link
        $sql = "SELECT ID FROM $wpdb->posts WHERE post_name = '$link' AND post_type = 'page'";
    	$page = $wpdb->get_results( $sql, OBJECT_K );
        if ( !empty( $page ) && false === strpos( $link, 'http' ) ) {
            $page = reset( $page );
            $link = get_page_link( $page->ID );
        }
        ob_start();
    
        ?><a href="<?php echo $link . $anchor ?>" class="<?php echo $class ?>"><?php echo Util::getNestedShortcodeContent( $content ) ?></a><?php
        return ob_get_clean();
    }
    
    function row( $atts, $content = null ) {
        $classes = is_array( $atts ) && array_key_exists( 'class', $atts ) ? ' ' . $atts['class'] : '';
        return $content ? sprintf( '<div class="row%s">%s</div>', $classes, Util::getNestedShortcodeContent( $content ) ) : '';
    }
    
    function subrow( $atts, $content = null ) {
       return $this->row( $atts, $content );
    }
    
    function col( $atts, $content = null ) {
        $span = reset( $atts );
        if ( !is_numeric( $span ) ) $span = '12';
        $classes = "col-sm-$span";
        $span_xs = next( $atts );
        if ( is_numeric( $span_xs ) ) {
            $classes .= " col-xs-$span_xs";
        }
        if ( array_key_exists( 'class', $atts ) ) {
            $classes .= ' ' . $atts['class'];
        }
        return sprintf( '<div class="%s">%s</div>', $classes, Util::getNestedShortcodeContent( $content ) );
    }
    
    function subcol( $atts, $content = null ) {
        return $this->col( $atts, $content );
    }
        
    function icon( $atts, $content = null ) {
        $name = reset( $atts );
        if ( !$name ) return '';
        $classes = array();
        $prefix = ( 0 === strpos( $atts[0], 'umfs' ) ) ? 'icon' : 'fa';
        foreach ( $atts as $index => $att ) {
            if( is_numeric( $index ) ) $classes[] = "$prefix-$att";
        }
        return sprintf( '<i class="%s %s"></i>', $prefix, implode( ' ', $classes ) );
    }
    
    function largeicon( $atts, $content = null ) {
        $class = 'element-color';
        if ( isset( $atts['class'] ) ) {
            $class = $atts['class'];
        }    
        array_push( $atts, '2x' );
        $icon = $this->icon( $atts );
        if ( isset( $atts['link'] ) ) {
            $icon = $this->icon( array( 'link' => $atts['link'] ), $icon );
        }
        return sprintf( '<div class="largeicon %s">%s</div>', $class, $icon );
    }
    
    
    function recent_posts( $atts, $content = null ) {
        global $recent_posts, $recent_posts_params;
        extract( shortcode_atts( array(
            'category' => '',
            'thumbnail_size' => 'medium',
            'thumbnail_position' => '',
            'limit' => '1',
            'large_title' => '',
            'color' => ''
        ), $atts ) );
        $image_column_map = array(
            'large' => 6,
            'medium' => 4,
            'thumbnail' => 2
        );
        $span = $image_column_map[$thumbnail_size];
        $container_class = 'recent-post';
        if ( $color ) $container_class .= " element-$color-override";
        $recent_posts_params = compact( 'thumbnail_size', 'thumbnail_position', 'large_title', 'color', 'image_column_map', 'span', 'container_class' );   
        $args = array( 
    	    'category_name' => $category,
    		'posts_per_page' => (int) $limit
    	);
        $recent_posts = get_posts( $args );
        $_post = $GLOBALS['post']; //store global post variable for later restoration
        ob_start();
        get_template_part( 'content/recent_posts', $thumbnail_position );
        $GLOBALS['post'] = $_post;
        return ob_get_clean();
    }
}