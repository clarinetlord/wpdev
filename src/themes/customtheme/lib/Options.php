<?php

namespace WPDev\Theme;

class Options {
    
    const KEY_PREFIX = 'theme_options_';
    
    static function get( $name, $default = '', $format = false ) {
        $option = get_option( self::KEY_PREFIX . $name, $default );
        if ( $format ) $option = apply_filters( 'the_content', $option );
        return $option;
    }
    
    static function getContent( $name, $format = false ) {
        $content = self::get( $name, '', $format );
        if ( !$format ) $content = do_shortcode( $content );
        return $content;
    }
    
    function getImage( $name, $default = '', $args = array( 'full' ) ) {
        $attachment =  self::get( $name, $default );
        if ( is_array( $attachment ) && isset( $attachment[0] ) && is_numeric( $attachment[0] ) ) {
            array_unshift( $args, $attachment[0] );
            return call_user_func_array( 'wp_get_attachment_image', $args );
        }
        return $default;
    }
    
}