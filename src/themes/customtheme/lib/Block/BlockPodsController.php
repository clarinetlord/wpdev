<?php

namespace WPDev\Theme\Block;

use WPDev\Theme\Base\BaseController;

class BlockPodsController extends BaseController {
        
    const KEY_PREFIX = 'pods_meta_';
    
    protected $theme_colors = array();
    protected $theme_colors_name_map = array();
    
    protected function hooks() {
        $taxonomies_full_caps = apply_filters( 'wpdev_pods_taxonomies_full_caps', array() );
        foreach ( $taxonomies_full_caps as $tax ) {
            add_filter( "pods_register_taxonomy_$tax", array( $this, 'registerTaxonomyFullCaps'), 10, 2 );
        }
        add_filter( 'pods_form_ui_field_pick_value', array( $this, 'fieldColor' ), 10, 2 );
        add_filter( 'pods_form_ui_field_pick_attributes', array( $this, 'fieldColorNameKey' ), 10, 2 );
        add_filter( 'pods_form_ui_field_label_attributes', array( $this, 'fieldColorLabel' ), 10, 2 );
    }
        
    function registerTaxonomyFullCaps( $options, $taxonomy ) {
        $options['capabilities'] = array(
                'assign_terms' => "assign_$taxonomy",
                'edit_terms' => "edit_$taxonomy",
                'manage_terms' => "manage_$taxonomy",
                'delete_terms' => "delete_$taxonomy"
        );
        return $options;
    }

    function fieldColor( $value, $name ) {
        if ( Block::isColorField( $name, self::KEY_PREFIX ) && empty( $this->theme_colors ) ) {
            $this->theme_colors = get_terms( Block::TAXONOMY_COLOR, array( 'hide_empty' => false, 'fields' => 'id=>slug' ) );
        }
        return $value;
    }
    
    
    function fieldColorNameKey( $attributes, $name ) {
        $term_id = (int) $attributes['value'];
        if ( Block::isColorField( $name, self::KEY_PREFIX ) && isset( $this->theme_colors[$term_id] ) ) {
            $color_slug = $this->theme_colors[$term_id];
            $this->theme_colors_name_map[$attributes['id']] = "admin-bg-$color_slug";
        }
        return $attributes;
    }
    
    
    function fieldColorLabel( $attributes, $name ) {
        if ( isset( $this->theme_colors_name_map[$attributes['for']] ) ) {
            $attributes['class'] .= ' ' . $this->theme_colors_name_map[$attributes['for']];
        }
        return $attributes;
    }
}