<?php 
    global $block; /* @var $block \WPDev\Theme\Block\Block */
    if ( !$block->hide_title ) {
        $format = $block->is_top_banner ? '<h1>%s</h1>' : '<h2>%s</h2>';
        printf( $format, $block->getTitle() );
    }
?>