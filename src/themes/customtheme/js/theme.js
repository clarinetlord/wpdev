jQuery(function($) {
    //add css hook to all post paragraphs with images (for print styles)
    $('.entry img').parents('.entry p').addClass('post-img');
    
    //unique class for each widget
    $('#sidebar .widget').each(function(i) {
        $(this).addClass('widget-'+(i+1));
    });

    //external links
    var domain = document.domain;
    $('a').each(function(i) {
        var absolute = this.href.match(/^http:\/\//);
        var internal = this.href.match(new RegExp(domain));
        if(absolute && !internal) this.target = '_blank';
    });
});
