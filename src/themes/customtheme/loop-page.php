<?php
/**
 * @package WordPress
 * @subpackage Custom_Theme
 */

/* Single Post / Page loop template */

?>
    <?php if (have_posts()) : ?>
    
        <?php while (have_posts()) : the_post(); ?>
            <?php do_action( 'wpdev_render_blocks_above' ); ?>
            <div id="post-<?php the_ID(); ?>" <?php post_class( 'col-sm-12' ); ?>>
            <h2><?php the_title(); ?></h2>
            <div class="entry">
                <?php the_content(); ?>
            </div>
            </div>
                <?php wp_link_pages(array('before' => '<div class="navigation"><div class="previous">Pages: ', 'after' => '</div></div>', 'next_or_number' => 'number')); ?>
                <?php edit_post_link('Edit this entry','<p>','</p>'); ?>

            <div class="col-sm-12">
            <?php comments_template(); ?>
            </div>
            <?php do_action( 'wpdev_render_blocks_below' ); ?>
        <?php endwhile; ?>
        
    <?php else : ?>
    
            <div class="page">
            <h2 class="center-block">Not Found</h2>
            <p class="center-block">Sorry, but you are looking for something that isn't here.</p>
            <?php get_search_form(); ?>
            </div>

    <?php endif; ?>
