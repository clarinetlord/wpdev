<?php 

namespace WPDev\Theme\Block;

class BlockView {
    
    public $location;
    
    function __construct( $location ) {
        $this->location = $location;
    }
    
    function renderTopBannerBlock() {
        global $post_element_color;
        $block_id = Block::getTopBannerBlockId();
        $block = Block::getInstance( $block_id );
        $post_element_color = $block->getPostElementColor();
        $block->render();
    }
    
    function renderBlocks() {
        $blocks_content = get_post_meta( get_the_ID(), "blocks_{$this->location}_content", true );
        if ( $blocks_content ) {
            foreach( explode( "\r\n", $blocks_content ) as $slug ) {
                echo call_user_func( array( Block::getClassName(), 'shortcode' ), compact( 'slug' ) );
            }
        }
    }
}